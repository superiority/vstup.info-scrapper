"use strict";

let fs = require('mz/fs');
let commonFile = "/common.txt";
let B = require("bluebird");
let newCommon = "/tasi.txt";

async function main(){
    let tasi = await fs.readFile("/data.txt", "utf-8");
    tasi = tasi.replace(/\t/g, " ").split("\r\n");
    tasi.pop();
    let log = await fs.readFile("./log/1485990235286.log", "utf-8");

    let files = await fs.readdir("/students");

    let univers = [];
    let universPeople = {};

    await B.each(files, async function(file){
        //console.log(`Processing file ${file}`);
        let data = await fs.readFile("/students/" + file, "utf8");
        let i = 0;
        for (i in tasi){
            let tasya = tasi[i];
            if (data.indexOf(tasya) !== -1){
                let univer = file.replace(".txt", "");
                univers.push(univer);
                !universPeople[univer] && (universPeople[univer] = []);
                universPeople[univer].push(tasya);
                //console.log("found one!" + tasya);
                //await fs.appendFile(newCommon, `${tasya} ${file}`, "utf8");
                //await fs.appendFile(newCommon, "\r\n", "utf8");
            }
        }
    });

    let links = {};

    univers.forEach(univer=>{

         log.substr(log.indexOf(univer) + univer.length, 72)
            .replace("Rating link: ", "")
            .replace(/.*#list/g, function (link){
                links[univer] = link;
                return "";
        });
    });
    let i = ""
    for (i in links){
        let string = `${universPeople[i].join(",")} ${i} ${links[i]}`
        await fs.appendFile("/all_data.txt", string, "utf8");
        await fs.appendFile("/all_data.txt", "\r\n", "utf8");
    }
};

(async function concatAll() {
    let files = await fs.readdir("/students");
    files.map(async function(file){
        console.log(`Processing file ${file}`);
        let data = await fs.readFile("/students/" + file, "utf8");
        data = data.split("\r\n").filter(row=>row.match(/Психологія/gi)).join("\r\n");
        await fs.appendFile(commonFile, data, "utf8");
    });
})();
