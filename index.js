"use strict";

let promisify = require("es6-promisify");
let request = promisify(require("request"));
let cheerio = require('cheerio');
let resolve = require('url').resolve;
let fs = require("mz/fs");
let B = require("bluebird");
let R = require("ramda");

const URL = 'http://vstup.info/2015/';
const MAX_PARALLEL_TASKS = 7;

let writeStream = fs.createWriteStream("./log/" + Date.now() + ".log", {defaultEncoding: 'utf8'});

let baseLog = console.log;
console.log = function(message){
    writeStream.write((new Date).toISOString() + "| " + message + "\n");
    baseLog((new Date).toISOString() + "| " + message);
};

async function getAllRatingLinks(universityLink){
    console.log("Getting rating links from " + universityLink);
    let response = await request(universityLink);
    if (response.statusCode !== 200) {
        console.error("Status not 200 for university: " + universityLink);
        return [];
    }

    let result;
    let ratingLinks = [];
    let course_regexp = /href="(\..{0,100}#list)"/g;

    let dennaTableHTML = cheerio.load(response.body, { decodeEntities: false })("#denna1").html();
    while (result = course_regexp.exec(dennaTableHTML)) {
        ratingLinks.push(resolve(URL, result[1]));
    }

    return ratingLinks;
}

async function getStudents(ratingLink){
    console.log("Getting students from " + ratingLink);
    let response = await request(ratingLink);
    if (response.statusCode !== 200) {
        console.error("Status not 200 for rating: " + ratingLink);
        return {
            info: {error: "Status not 200 for rating: " + ratingLink},
            students: []
        }
    }

    let rating = response.body;

    let result;
    let students = [];
    let info = {};
    //let student_regexp = /<tr title="Зараховано. Вітаємо!"><td style="background:#b4caeb">\s*.{0,4}<\/td><td style="background:#b4caeb">(.{1,50})<\/td>/g;
    let student_regexp = /<td style="background:#b4caeb">\d*<\/td><td style="background:#b4caeb">(\D{1,40})<\/td>/g;
    while (result = student_regexp.exec(rating)) {
        students.push(result[1]);
    }

    //UNIVER
    let univer_detect_regexp = /<div class="title-page">[\s\S]*?>([\s\S]*?)<\/h3>[\s\S]*?<p>[\s\S]*?<br>(.*?)</g;
    while (result = univer_detect_regexp.exec(rating)) {
        info.univer = result[1];
        info.napryam = result[2];
    }

    !info.univer && (info.univer = "") && console.log(`Univer not detected! Link: ${ratingLink}`);
    !info.napryam && (info.napryam = "");

    if (students.length > 0) {
        //let fileName = "/students/" + info.univer.replace(/\s/g, "_") + "-" + info.napryam.replace(/<br>/g,"").replace(/\s/g, "_").replace(/:/g,"") + "_" + Date.now() + ".txt";
        let fileName = "/students/" + info.univer.replace(/\s/g, "_") + "_" + Date.now() + ".txt";
        console.log("Writing students in " + fileName + "\r\nRating link: " + ratingLink);
        students = students.map((str)=>`${str}\t${info.univer}\t${info.napryam}\t${ratingLink}\r\n`);
        await fs.writeFile(fileName, students.join(""), "utf-8");
    } else {
        console.log(`On link ${ratingLink} no one entry university.`);
    }

    return {info: info, students: students};
}

(async function main(){
    console.log(`Starts on ${(new Date().toISOString())}`);

    let body = await fs.readFile("./test.html", "utf-8");
    let vnz_regexp = /href="(\..*#vnz)"/g;
    let result, universitiesLinks = [];
    while (result = vnz_regexp.exec(body)) {
        universitiesLinks.push(resolve(URL, result[1]));
    }

    //universitiesLinks = [universitiesLinks.shift()];

    let ratingLinks = R.flatten(await B.map(universitiesLinks, getAllRatingLinks, {concurrency: MAX_PARALLEL_TASKS}));

    //let ratingLinks = ["http://vstup.info/2015/228/i2015i228p261919.html#list"];

    await B.map(ratingLinks, getStudents, {concurrency: MAX_PARALLEL_TASKS});

    console.log("Done.")

})().catch(e=>console.log(e.stack || e));

process.on('exit', (code) => {
    console.log(`About to exit with code: ${code}`);
    console.log(`Ends on ${(new Date().toISOString())}`)
});
